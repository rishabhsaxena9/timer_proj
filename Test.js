import React, { useEffect, useState } from 'react';
import { Animated, Image, Text, View, TouchableOpacity, Dimensions, StyleSheet, TextInput, ScrollView, Keyboard } from 'react-native';
const {height, width} = Dimensions.get('window');
import Timer from "./Timer";
import messaging from '@react-native-firebase/messaging';

const Test = () => {
    const [list, setList] = useState([]);
    const [itemVal, setItemVal] = useState(false);
    const [edit, setEdit] = useState('');
    const [showEdit, setShowEdit] = useState(false);
    const [selKey, setSelKey] = useState(0);
    const [filterText, setFilterText] = useState('');
    const [curList, setCurList] = useState([]);

    useEffect(() => {
        setCurList(list)
    }, [list])

    useEffect(() => {
        messaging().registerDeviceForRemoteMessages();

        // Get the token
        const token = messaging().getToken();
        console.log(token, 'token-->>')
    }, [])

    const addList = (time) => {
        let timeObj = {
            title: "listItem"+list.length,
            time: time,
            uniqueKey: "unique"+list.length
        }
        setItemVal(!itemVal);
        list.push(timeObj);
    }

    const removeItem = (item) => {
        let filtered = list.filter(i => i.uniqueKey !== item.uniqueKey);
        setList(filtered);
    }

    const onChangeText = (text) => {
        setEdit(text);
    }

    const saveItem = (item) => {
        if(edit.length == 0){
            alert('Title cannot be empty')
            return
        }
        item.title = edit;
        list.map(function(i) { return i.uniqueKey == item.uniqueKey ? item : i });
        setShowEdit(false)
        setEdit("")
    }

    const filterList = (text) => {
        setFilterText(text)
        let newList = list.filter(i=>i.title.includes(text));
        setCurList(newList);
    }

    const clearSearch = () => {
        setFilterText("");
        setCurList(list);
        Keyboard.dismiss();
    }

    return (
        <View style={styles.main}>
            {/* <Timer 
                updateTime={(time)=>addList(time)}
            />
            <View style={styles.listBox}>
                {list.length > 0 &&
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 16 }}>
                        <TextInput
                            style={[styles.itemText, { borderRadius: 8, borderWidth: 1, height: 40, paddingHorizontal: 16, width: width - 80, borderColor: '#F5FCFF'}]}
                            onChangeText={text => filterList(text)}
                            placeholder={'Search...'}
                            value={filterText}
                        />
                        <TouchableOpacity onPress={()=>clearSearch()} disabled={filterText == ""}>
                            <Text style={[styles.optText, { marginLeft: 0}]}>Clear</Text>
                        </TouchableOpacity>
                    </View>
                }
                <ScrollView showsVerticalScrollIndicator={false}>
                    {curList.length > 0 && <Text style={styles.btnText}>List ---</Text>}
                    {curList.length > 0 && curList.map((item, index) => {
                        return (
                            <View style={styles.listEachItem} key={index}>
                                <View>
                                    {showEdit && selKey == index ?
                                        <TextInput
                                            style={styles.itemText}
                                            onChangeText={text => onChangeText(text, item, index)}
                                            value={edit}
                                        />
                                        :
                                        <TouchableOpacity onPress={()=> (setShowEdit(true),setEdit(item.title), setSelKey(index))}>
                                            <Text style={styles.itemText}>{item.title}</Text>
                                        </TouchableOpacity>
                                    }
                                    <Text style={styles.itemSubText}>{item.time}</Text>
                                </View>
                                <View style={{flexDirection: 'row'}}>
                                    {showEdit && selKey == index &&
                                        <TouchableOpacity onPress={()=>saveItem(item)}>
                                            <Text style={styles.optText}>Save</Text>
                                        </TouchableOpacity>
                                    }
                                    {showEdit && selKey == index &&
                                        <TouchableOpacity onPress={()=>setShowEdit(false)}>
                                            <Text style={[styles.optText, { marginLeft: 20}]}>Cancel</Text>
                                        </TouchableOpacity>
                                    }
                                    <TouchableOpacity onPress={()=>removeItem(item)}>
                                        <Text style={[styles.optText, { marginLeft: 20}]}>Delete</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        )
                    })}
                </ScrollView>
            </View> */}
        </View>
    );
}

export default Test;

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: '#0E20A7',
        padding: 16,
        paddingTop: 40
    },
    header: {
        height: height * 0.1,
        width: '100%',
        backgroundColor: 'yellow',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8
    },
    btnText: {
        fontSize: 20,
        fontWeight: "bold",
        paddingTop: 16,
        color: "#fff"
    },
    listBox: {
        flex: 0.8
    },
    listEachItem: {
        width: width - 32,
        height: 60,
        backgroundColor: 'transparent',
        borderColor: '#F5FCFF',
        borderWidth: 1,
        marginTop: 16,
        borderRadius: 8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 16
    },
    itemText: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#fff"
    },
    itemSubText: {
        fontSize: 12,
        color: "#fff",
        marginTop: 4
    },
    optText: {
        fontSize: 14,
        fontWeight: "bold",
        color: "#fff"
    }
});

