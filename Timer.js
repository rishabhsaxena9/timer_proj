import React, { useEffect, useState, Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
 
export default class Timer extends Component {
 
  constructor(props) {
    super(props);
 
    this.state = {
      timer: null,
      minutes_Counter: '00',
      seconds_Counter: '00',
      startDisable: false,
      showStop: false
    }
  }
 
  componentWillUnmount() {
    clearInterval(this.state.timer);
  }
 
  onButtonStart = () => {
    this.setState({showStop: true})
 
    let timer = setInterval(() => {
 
      var num = (Number(this.state.seconds_Counter) + 1).toString(),
        count = this.state.minutes_Counter;
 
      if (Number(this.state.seconds_Counter) == 59) {
        count = (Number(this.state.minutes_Counter) + 1).toString();
        num = '00';
      }
 
      this.setState({
        minutes_Counter: count.length == 1 ? '0' + count : count,
        seconds_Counter: num.length == 1 ? '0' + num : num
      });
    }, 1000);
    this.setState({ timer });
    this.setState({startDisable : true});
  }
 
 
  onButtonStop = () => {
    clearInterval(this.state.timer);
    let time = this.state.minutes_Counter+" : "+this.state.seconds_Counter;
    this.props.updateTime(time);
    this.setState({startDisable : false});
    this.onButtonClear();
    this.setState({showStop: false});
  }
 
 
  onButtonClear = () => {
    this.setState({
      timer: null,
      minutes_Counter: '00',
      seconds_Counter: '00',
    });
  }
 
  render() {
 
    return (
      <View style={styles.MainContainer}>
 
        <Text style={styles.counterText}>{this.state.minutes_Counter} : {this.state.seconds_Counter}</Text>
 
        <TouchableOpacity
          onPress={this.onButtonStart}
          activeOpacity={0.6}
          style={[styles.button, { backgroundColor: this.state.startDisable ? '#B0BEC5' : '#FF6F00' }]} 
          disabled={this.state.startDisable} >
 
          <Text style={styles.buttonText}>START</Text>
 
        </TouchableOpacity>
 
        <TouchableOpacity
          onPress={this.onButtonStop}
          disabled={!this.state.showStop}
          activeOpacity={0.6}
          style={[styles.button, { backgroundColor:  !this.state.showStop ? "#B0BEC5" : "#FF6F00"}]} >
 
          <Text style={styles.buttonText}>STOP</Text>
 
        </TouchableOpacity>
 
      </View>
 
    );
  }
}
 
 
 
const styles = StyleSheet.create({
  MainContainer: {
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  button: {
    width: '80%',
    paddingTop:8,
    paddingBottom:8,
    borderRadius:7,
    marginTop: 10
  },
  buttonText:{
      color:'#fff',
      textAlign:'center',
      fontSize: 20
  },
  counterText:{
 
    fontSize: 28,
    color: '#000'
  }
});